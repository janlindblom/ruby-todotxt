require 'thor'
require 'todo-txt'

module TodoTxt
  class CLI < Thor

    desc "add THING I NEED TO DO", "Adds THING I NEED TO DO to your todo.txt file on its own line."
    long_desc <<-LONGDESC
    Adds THING I NEED TO DO to your todo.txt file on its own line.
      Project and context notation optional.
      Quotes optional.
    LONGDESC
    def add(thing)
      puts "add #{thing}"
    end

    desc "addm THINGS I NEED TO DO", "Adds FIRST THING I NEED TO DO to your todo.txt on its own line and Adds SECOND THING I NEED TO DO to you todo.txt on its own line."
    long_desc <<-LONGDESC
      Adds FIRST THING I NEED TO DO to your todo.txt on its own line and
      Adds SECOND THING I NEED TO DO to you todo.txt on its own line.
      Project and context notation optional.
    LONGDESC
    def addm(things=[])
      puts "addm #{things}"
    end

    desc 'addto DEST "TEXT TO ADD"', "Adds a line of text to any file located in the todo.txt directory."
    long_desc <<-LONGDESC
      Adds a line of text to any file located in the todo.txt directory.
      For example, addto inbox.txt "decide about vacation"
    LONGDESC
    def addto(dest, text)
      puts "addto #{dest} #{text}"
    end

    desc 'append ITEM# "TEXT TO APPEND"', 'Adds TEXT TO APPEND to the end of the task on line ITEM#.'
    def append(item, text_to_append)
      puts "append #{item} #{text_to_append}"
    end

    desc "archive", "Moves all done tasks from todo.txt to done.txt and removes blank lines."
    def archive

    end

    desc "command...", ''
    def command

    end

    desc "deduplicate...", ''
    def deduplicate

    end

    desc "del...", ''
    def del

    end

    desc "depri...", ''
    def depri

    end

    desc "do...", ''
    def do

    end

    desc "list [TERM...]", "Displays all tasks that contain TERM(s) sorted by priority with line numbers."
    long_desc <<-LONGDESC
      Displays all tasks that contain TERM(s) sorted by priority with line
      numbers.  Each task must match all TERM(s) (logical AND); to display
      tasks that contain any TERM (logical OR), use
      "TERM1\|TERM2\|..." (with quotes), or TERM1\\\|TERM2 (unquoted).
      Hides all tasks that contain TERM(s) preceded by a
      minus sign (i.e. -TERM). If no TERM specified, lists entire todo.txt.
    LONGDESC
    def list(terms=[])
      puts "list #{terms}"
    end

    desc "listall [TERM...]", ''
    long_desc <<-LONGDESC
      Displays all the lines in todo.txt AND done.txt that contain TERM(s)
      sorted by priority with line  numbers.  Hides all tasks that
      contain TERM(s) preceded by a minus sign (i.e. -TERM).  If no
      TERM specified, lists entire todo.txt AND done.txt
      concatenated and sorted.
    LONGDESC
    def listall(terms=[])
      puts "listall #{terms}"
    end

    desc "listcon [TERM...]", ''
    long_desc <<-LONGDESC
      Lists all the task contexts that start with the @ sign in todo.txt.
      If TERM specified, considers only tasks that contain TERM(s).
    LONGDESC
    def listcon(terms=[])
      puts "listcon #{terms}"
    end
  end
end