# todo.sh, but Ruby: A todo.txt CLI

This is a [todo.txt](https://github.com/ginatrapani/todo.txt-cli/) CLI implemented in Ruby as a Ruby gem with a bundled executable called `todo` which has the same features as those that come with the official todo.txt CLI `todo.sh`.

## Installation

Install the gem:

```bash
$ gem install ruby-todotxt
```

## Usage

TODO: Usage instructions goes here

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/janlindblom/ruby-todotxt. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the TodoTxt project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://bitbucket.org/janlindblom/ruby-todotxt/src/master/CODE_OF_CONDUCT.md).
